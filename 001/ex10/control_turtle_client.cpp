/*
 * 消息管道为/turtle1/cmd_vel 消息类型为geometry_msgs::Twist
 * 节点名为  /turtlesim
 */

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"

void cmdCallBack(const geometry_msgs::Twist& msg){
    //将收到的消息打印出来
    ROS_INFO("The message I received was:[msg.linear.x = %0.2f m/s, msg.linear.y = %0.2f m/s, msg.linear.z = %0.2f m/s, msg.angular.z = %.2f rad/s]"
            , msg.linear.x,  msg.linear.y, msg.linear.z, msg.angular.z);
}

int main(int argc, char **argv){
    //初始化ros节点
    ros::init(argc, argv, "Subscriber");//节点名也可以写成/turtlesim
    //创建节点句柄
    ros::NodeHandle node;

    //创建一个Subscriber，消息管道为/turtle1/cmd_vel,注册回调函数为cmdCallBack
    ros::Subscriber sub = node.subscribe("/turtle1/cmd_vel", 1000, cmdCallBack);
    ros::spin();

    return 0;
}

