//以 /turtle1/cmd_vel消息管道，以geometry_msgs::Twist传递消息
// /teleop_turtle，该为发布者的节点名
// linear线性的  angular角度的
//%0.2f打印geometry_msgs::Twist类型
#include "ros/ros.h"
#include "geometry_msgs/Twist.h"

int main(int argc, char **argv){
    //初始化ros节点
    ros::init(argc, argv, "Publisher");//也可以将Publisher改为/teleop_turtle
    //创建节点句柄
    ros::NodeHandle node;

    //创建一个发布者Publisher，消息管道/turtle1/cmd_vel，消息类型geometry_msgs::Twist
    ros::Publisher pub = node.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel",1000);

    //设置一个循环频率每秒5次
    ros::Rate loop_rate(5);

    //计数发布次数
    int count = 0;
    while (ros::ok()){
        //初始化消息类型geometry_msgs::Twist
        geometry_msgs::Twist msg;
        msg.linear.x = 1;
        msg.linear.y = 0;
        msg.linear.z = 0;
        msg.angular.z = 0;

        //发布消息
        pub.publish(msg);
        
        //初始化消息类型geometry_msgs::Twist
        geometry_msgs::Twist msg;
        msg.linear.x = 0;
        msg.linear.y = 1;
        msg.linear.z = 0;
        msg.angular.z = 0;

        //发布消息
        pub.publish(msg);
        
        //初始化消息类型geometry_msgs::Twist
        geometry_msgs::Twist msg;
        msg.linear.x = 0;
        msg.linear.y = 0;
        msg.linear.z = 1;
        msg.angular.z = 0;

        //发布消息
        pub.publish(msg);

        //发布成功在该节点终端显示
        //已成功发布第%d条消息,发布的内容为%0.2f打印geometry_msgs::Twist类型
        ROS_INFO("Successfully published message %d with:[msg.linear.x = %0.2f m/s, msg.linear.y = %0.2f m/s, msg.linear.z = %0.2f m/s, msg.angular.z = %.2f rad/s]"
                ,count , msg.linear.x,  msg.linear.y, msg.linear.z, msg.angular.z);

        loop_rate.sleep();
        ++count;
    }
    return 0;
}

