#!/usr/bin/env python

from __future__ import print_function

from beginner_tutorials.srv import service_full_name,service_full_nameResponse
import rospy

def handle_add_three_strs(req):
    print("Returning [%s + %s]"%(req.first_name, req.name))
    return service_full_nameResponse(req.first_name+req.name)
    
def full_name_sum_server():
    rospy.init_node('full_name_sum_server')
    s = rospy.Service('add_two_ints', service_full_name, handle_add_three_strs)
    print("Please write down your first name and name.")
    rospy.spin()

if __name__ == "__main__":
    full_name_sum_server()
