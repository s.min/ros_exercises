#!/usr/bin/env python

from __future__ import print_function

import sys
import rospy
from beginner_tutorials.srv import *


def full_name_sum_client(first_name, name):
    rospy.wait_for_service('add_two_ints')
    try:
        add_two_ints = rospy.ServiceProxy('add_two_ints', full_name_sum_service)
        resp1 = add_two_ints(first_name, name)
        return resp1.full_name
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def usage():
    return "%s [x y z]"%sys.argv[0]

if __name__ == "__main__":
    if len(sys.argv) == 3:
        name = sys.argv[2]
        first_name = sys.argv[1]
    else:
        print(usage())
        sys.exit(1)
    print("Requesting %s+%s"%(first_name, name))
    print("%s + %s = %s"%(first_name, name, full_name_sum_client(first_name, name)))
