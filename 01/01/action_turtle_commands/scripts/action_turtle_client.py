#! /usr/bin/env python
# -*- coding:utf-8 -*-
from __future__ import print_function
import rospy
import actionlib
import sys
import roslib
from action_turtle_commands.msg import message_turtle_commandsAction,message_turtle_commandsGoal,message_turtle_commandsResult,message_turtle_commandsFeedback
#from action_turtle_commands.action import message_turtle_commands

roslib.load_manifest("actionlib_tutorials")

def action_client():
    # Creates the SimpleActionClient, passing the type of the action
    # (FibonacciAction) to the constructor.
    client = actionlib.SimpleActionClient('action_turtle', message_turtle_commandsAction)

    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()

    # Creates a goal to send to the action server.
    goal = message_turtle_commandsGoal(command=command,s=s,angle=angle)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()  # A FibonacciResult

def printFeedback(f):
    print(f)

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('action_client_py')
        '''if len(sys.argv) == 4:
            command=str(sys.argv[1])
            s = int(sys.argv[2])
            angle = int(sys.argv[3])'''
        command=str(input())
        s=int(input())
        angle=int(input())
        #print(command,type(command),s,type(s),angle,type(angle))
        res = action_client(command,s,angle)
        print("finish ! turtle  %s distance=%d(s) angle=%d"%(command,s,angle))
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr) 
