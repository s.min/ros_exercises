#! /usr/bin/env python
# -*- coding:utf-8 -*-
import rospy
import actionlib
from action_turtle_commands.msg import message_turtle_commandsAction,message_turtle_commandsGoal,message_turtle_commandsResult,message_turtle_commandsFeedback
from geometry_msgs.msg import Twist
import roslib
 
class TurtleAction(object):
    _feedback = message_turtle_commandsFeedback()
    _result = message_turtle_commandsResult()
    
    def __init__(self, name):
        self._sction_name = name
        self._as = actionlib.SimpleActionServer(self._sction_name, message_turtle_commandsAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
    
    ##to do the order    
    def execute_cb(self, goal):
        ## helper variables
        #1hz publish order
        r = rospy.Rate(1)
        success = True
        
        # publish info to the console for the user
        rospy.loginfo('%s: Executing, creating action sequence of order %i with seeds %i, %i' % (self._action_name,goal.command,goal.s,goal.angle))
        
        
        # start executing the action
        '''for i in range(1, goal.order):
        # check that preempt has not been requested by the client
            if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
                break
            self._feedback.sequence.append(self._feedback.sequence[i] + self._feedback.sequence[i-1])
            # publish the feedback
            self._as.publish_feedback(self._feedback)
            # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
            r.sleep()'''
        pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
        twist=Twist()
        twist.linear.x = 0.0
        twist.linear.y = 0.0
        twist.linear.z = 0.0
        twist.angular.x = 0.0
        twist.angular.y = 0.0
        twist.angular.z = 0.0
        if(goal.command=="turn_left"):
            rospy.loginfo("receive %s command"%(goal.command))
            twist.angular.z=goal.angle
            pub.publish(twist)
            r.sleep()
        elif(goal.command=="turn_right"):
            rospy.loginfo("receive %s command"%(goal.command))
            twist.angular.z=-goal.angle
            pub.publish(twist)
            r.sleep()
        elif(goal.command=="forward"):
            rospy.loginfo("receive %s command"%(goal.command))
            twist.linear.x=1.0
            n=goal.s
            for i in range(n):
                pub.publish(twist)
                self._as.publish_feedback(message_turtle_commandsFeedback(i+1))
                r.sleep()         
        else:
            rospy.loginfo("The command entered is wrong!")
            success=False
            
        if success:
            self._result.result= success
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('action_turtle')
    server = TurtleAction(rospy.get_name())
    rospy.spin()
