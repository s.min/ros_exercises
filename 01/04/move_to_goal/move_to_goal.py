#!/usr/bin/env python

from __future__ import print_function
import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from math import pow, atan2, sqrt


class Move(object):

    def __init__(self):
        rospy.init_node('move_to_goal', anonymous=True)
        self.sub= rospy.Subscriber('/turtle1/pose',Pose, self.poseNow)
        self.pub= rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
        self.pose=Pose()
        self.r = rospy.Rate(10)
        
    def poseNow(self,data):
        self.pose = data
        self.pose.x = self.pose.x
        self.pose.y = self.pose.y
        self.pose.theta = self.pose.theta
        #print("turtle pose nowww: x=%f,y=%f,theta=%f"%(self.pose.x,self.pose.y,self.pose.theta))

    def poseCallBack(self):
        print("turtle pose now: x=%f,y=%f,theta=%f"%(self.pose.x,self.pose.y,self.pose.theta))
        
    def distance(self,g_x,g_y):
        return sqrt(pow(g_x-self.pose.x,2)+pow(g_y-self.pose.y,2))
    
    
    def execute_move(self):
        pose_goal=Pose()
        pose_goal.x=float(input())
        pose_goal.y=float(input()) 
        pose_goal.theta=float(input())
        print("pose goal: x=%f,y=%f,theta=%f"%(pose_goal.x,pose_goal.y,pose_goal.theta))
        self.poseCallBack()
        twist=Twist()
        twist.linear.x = 0.0
        twist.linear.y = 0.0
        twist.linear.z = 0.0
        twist.angular.x = 0.0
        twist.angular.y = 0.0
        twist.angular.z = 0.0
        while(self.distance(pose_goal.x,pose_goal.y)>0.1):
            twist.linear.x=1
            twist.angular.z=atan2(pose_goal.y-self.pose.y,pose_goal.x-self.pose.x)-self.pose.theta
            self.pub.publish(twist)
            self.r.sleep()
        twist.linear.x = 0.0
        twist.angular.z = 0.0
        self.pub.publish(twist)
        #while (goal_pose.theta-self.pose.theta)>0.01 :
        #    twist.angular.z=1.0
        #    self.pub.publish(twist)
        #    self.r.sleep()
        #self.r.sleep()
        print("Finish move")
        #rospy.spin()
        
        
'''def execute_move():
    rospy.init_node('move_to_goal', anonymous=True)
    sub= rospy.Subscriber('/turtle1/Pose',Pose, self.execute_now)
    pub= rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    pose=Pose()
    #r = rospy.Rate(1)''' 

if __name__ == "__main__":
    move=Move()
    move.execute_move()
